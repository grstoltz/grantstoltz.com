import React from 'react'

const Banner = props => (
  <section id="banner" className="major">
    <div className="inner">
      <header className="major">
        <h1>Hi, I'm Grant</h1>
      </header>
      <div className="content">
        <p>Welcome to my website!</p>
        <ul className="actions">
          <li>
            <a href="#contact" className="button next scrolly">
              Get in Touch!
            </a>
          </li>
        </ul>
      </div>
    </div>
  </section>
)

export default Banner
