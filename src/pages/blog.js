import React from 'react'
import { Link, graphql } from 'gatsby'
import Helmet from 'react-helmet'
import Img from 'gatsby-image'
import Layout from '../components/layout'
import GenericBanner from '../components/GenericBanner'

const BlogIndex = ({ data }) => {
  const { edges: posts } = data.allMarkdownRemark
  return (
    <Layout>
      <Helmet>
        <title>Grant Stoltz | Blog</title>
        <meta name="description" content="The blog of Grant Stoltz" />
      </Helmet>

      <GenericBanner
        header="Blog"
        content='Remember when these were called "web logs"? '
      />
      <div id="main">
        <section id="two" className="spotlights">
          {posts.map(({ node: post }, index) => {
            const { frontmatter } = post
            const excerpt =
              post.html
                .replace(/<(.|\n)*?>/g, '')
                .substring(0, 100)
                .trim() + '...'

            let postImage
            try {
              postImage = frontmatter.image.childImageSharp.fluid
            } catch (e) {
              postImage = null
            }
            return (
              <section
                key={index}
                style={{
                  borderBottomStyle: 'solid',
                  borderBottomColor: 'grey',
                  borderBottomWidth: '2px',
                }}
              >
                <Link to={`${frontmatter.path}`} className="image">
                  {postImage === null ? (
                    <div style={{ height: '100%' }} />
                  ) : (
                    <Img style={{ height: '100%' }} fluid={postImage} />
                  )}
                </Link>
                <div className="content">
                  <div className="inner">
                    <header className="major">
                      <Link
                        style={{ textDecoration: 'none' }}
                        to={`${frontmatter.path}`}
                      >
                        <h3>{frontmatter.title}</h3>
                      </Link>
                    </header>
                    <p>{excerpt}</p>
                    <p>Date Posted: {frontmatter.date}</p>
                    <ul className="actions">
                      <li>
                        <Link to={`${frontmatter.path}`} className="button">
                          Read more
                        </Link>
                      </li>
                    </ul>
                  </div>
                </div>
              </section>
            )
          })}
        </section>
      </div>
    </Layout>
  )
}

export default BlogIndex

export const pageQuery = graphql`
  query {
    site {
      siteMetadata {
        title
        description
      }
    }
    allMarkdownRemark(sort: { fields: [frontmatter___date], order: DESC }) {
      edges {
        node {
          id
          html
          frontmatter {
            title
            date(formatString: "MMMM DD, YYYY")
            path
            image {
              childImageSharp {
                resize(width: 1500, height: 1500) {
                  src
                }
                fluid(maxWidth: 786) {
                  ...GatsbyImageSharpFluid
                }
              }
            }
          }
        }
      }
    }
  }
`
