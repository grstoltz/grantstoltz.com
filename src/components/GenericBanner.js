import React from 'react'

const GenericBanner = (props) => (
    <section id="banner" className="style2">
        <div className="inner">
            <header className="major">
                <h1>{props.header}</h1>
            </header>
            <div className="content">
                <p>{props.content}</p>
            </div>
        </div>
    </section>
)

export default GenericBanner;
