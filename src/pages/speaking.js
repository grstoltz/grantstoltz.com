import React from 'react'
import { Link } from 'gatsby'
import Helmet from 'react-helmet'
import Layout from '../components/layout'

import GenericBanner from '../components/GenericBanner'

import OTA from '../assets/images/OTA-Presentation-min.png'
import Summit2017 from '../assets/images/Simpleview-Summit-2017.jpg'
import GoogleWebinar from '../assets/images/Google-Webinar.png'

const Generic = props => (
  <Layout>
    <Helmet>
      <title>Grant Stoltz | Speaking Engagements</title>
      <meta
        name="description"
        content="Interested in what I have to say about digital marketing? Find my speaking engagements I've done here."
      />
    </Helmet>

    <GenericBanner
      header="Speaking Engagements"
      content="Interested in what I have to say about digital marketing? Find my speaking engagements I've done here."
    />
    <div id="main">
      {/* <section id="one">
                <div className="inner">
                    <header className="major">
                        <h1 style={{}}>Speaking Engagements</h1>
                    </header>
                    <p>Donec eget ex magna. Interdum et malesuada fames ac ante ipsum primis in faucibus. Pellentesque venenatis dolor imperdiet dolor mattis sagittis. Praesent rutrum sem diam, vitae egestas enim auctor sit amet. Pellentesque leo mauris, consectetur id ipsum sit amet, fergiat. Pellentesque in mi eu massa lacinia malesuada et a elit. Donec urna ex, lacinia in purus ac, pretium pulvinar mauris. Curabitur sapien risus, commodo eget turpis at, elementum convallis elit. Pellentesque enim turpis, hendrerit.</p>
                </div>
            </section> */}
      <section id="two" className="tiles">
        <article style={{ backgroundImage: `url(${OTA})` }}>
          <header className="major">
            <h3>Ohio Travel Association - Focus on Tourism</h3>
            <p>The Power of Facebook Custom Audiences</p>
          </header>
          <Link to="/landing" className="link primary" />
        </article>
        <article style={{ backgroundImage: `url(${Summit2017})` }}>
          <header className="major">
            <h3>Simpleview Summit 2017</h3>
            <p>Dude Where's My Content</p>
          </header>
          <Link to="/landing" className="link primary" />
        </article>
        <article style={{ backgroundImage: `url(${GoogleWebinar})` }}>
          <header className="major">
            <h3>Google Partners Webinar</h3>
            <p>The Digital Marketing Cycle</p>
          </header>
          {
            // eslint-disable-next-line
          }
          <a
            className="link primary"
            href="https://www.youtube.com/watch?v=5jJmjFYpHUw"
          />
        </article>
      </section>
    </div>
  </Layout>
)

export default Generic
