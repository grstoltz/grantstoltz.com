---
title: Building a Blog - Exploring Gatsby.js
date: "2019-02-12"
path: "/blog/building-a-blog/"
description: "Deciding to scrap a website and start fresh is a daunting task. Gatbsy makes this process much more pleasant."
image: "christopher-burns-368617-unsplash.jpg"
---

Deciding to scrap a website and start fresh is a daunting task. Over the years grantstoltz.com has gone through dozens of iterations. Everything from poorly made static HTML pages, Flash sites (yes, you read that right), and even redirects to Tumblr (yikes), have graced the presence of grantstoltz.com's DNS. After a long period of inactivity and my domain simply redirecting to a perpetual "Under Construction" splash page (early 2000s web development habits die hard), I decided it was time to put on my marketer pants and actually have a functioning website where I can post blogs and projects I've worked on. Now I'm a big fan of marketing and a big fan of technology (some people even call that MarTech) so when I began exploring how I would want to rebuild my website, I had a few requirements from both a technical and a marketing perspective.

###The Requirements

1.  It's Fast - With Google leaning on pagespeed more and more as a ranking factor, great pagespeed is paramount to having blogs and pages rank well in Google (amongst other things).

2.  It's Flexible - If I want to make a page look a certain way, I want the ability to do so easily. I didn't want to be encumbered by templated pages such as on Wix or Squarespace and have to write some serious CSS just to make a custom page layout work.

3.  It's User (me) Friendly - This is a combination of point one and two. I want everything to be easy. One of my favorite mantras is from Tim Ferriss who quite often poses the rhetorical question ["What would this look like if it were easy?"](https://tim.blog/2017/10/03/tribe-of-mentors/). I chose to follow this when vetting tools and ways to build my website. Sometimes a CMS can make one thing very easy, like writing content, but can make other tasks like optimizing for pagespeed a headache.

With all that said, I dove into my search.

##The Search Begins

Initially I considered making everything with static HTML, CSS, and Javascript where I would hand write every line of code of every page and spend days and days crafting every page on the site to be a well oiled machine. While the pagespeed would be great and I would have virtually unlimited options of how the site would look and operate, creating new pages, updating components, and making changes to things like the sitemap or meta tags would just add extra labor with little benefit to the bottom line. So out the window that idea went.

Then I threw around the idea of creating a full fledged [Create-React-App](https://facebook.github.io/create-react-app/) application. Being familiar with the MERN stack (MongoDB, Express, React, Node) this would be fairly easy for me to build, plus React is meant for building user interfaces, so tasks creating reusable components are baked right into it. Plus React has a great community, so packages for sitemap and meta tag generators are readily available. However after building out a few pages and looking at the pagespeed, the future wasn't looking bright. The downside of React is that the routes are rendered client-side with Javascript on an empty index.html. And none of the images were optimized; in a few cases, uncached images were being sent on every page load.

And yes, there are definitely some solutions to solve those problems out there and I love working with React but for my personal site where I was more concerned with writing that finding additional pagespeed optimization solutions, React took a backseat. So with that, I began looking for another solution.

##Enter Gatsby

After doing some Googling and stumbling across static site generators such as Jekyll, I found the Goldilocks solution (not too hot, not too cold, just right) to my problems. The solution came in the form of a static website generator called [Gatsby.js](https://www.gatsbyjs.org/). Simply put (as said on their website), "Gatsby is a blazing fast modern site generator for React". Every route is built into a static HTML page, and all the Webpack optimizations are handled by the library's developers. Blogs can be written in Markdown and page templating is done using React. To make things even easier tasks like Markdown transformation and image resizing are all just plugins that are added in with just a line or two of code.

After taking a look at the docs I was off to the races. Given my experience with React, creating page templates was a breeze. The hardest thing for me was learning some of the built-in GraphQL queries which are used to populate the data on the pages on build - that was a new world to me. The usage of GraphQL acts almost as a wrapper and allows for you to plug-in other data sources  like a decoupled CMS such as [Headless Wordpress](https://www.gatsbyjs.org/blog/2018-01-22-getting-started-gatsby-and-wordpress/) or [Contentful](https://www.contentful.com/r/knowledgebase/gatsbyjs-and-contentful-in-five-minutes/).

Even with the ability to plug into nice user interfaces from ConteI chose to keep all my pages and blog content local, mostly to keep things simple, but also to maintain full control over my posts' content. If there are any edge cases of things that come up that I may want to add down the line, I want to be able to fully and easily implement them. Also, adding in another data source adds in another level of complexity, no matter how "easy" it may seem. Remember, "What would this look like if it were easy?".

##Let's Deploy

Finally, the wonderful world of deployment. Let me preface this by saying there are many different ways to deploy a Gatsby site. The beauty of Gatsby is similar to building a Create-React-App site you are given a /public directory where your entire site exists after running the "build" command. From there you can easily deploy it to places such as [Netlify](https://www.gatsbyjs.org/docs/hosting-on-netlify/), or [AWS Amplify](https://www.gatsbyjs.org/blog/2018-08-24-gatsby-aws-hosting/). I however chose to deploy it to a [DigitalOcean](https://www.digitalocean.com/) droplet where I have an NGINX server running. Why not use one of the easy deployment services? Yep you guessed it, full control. On this NGINX server I also have a Node application running that takes in the POST requests for the contact form at the bottom of the page. So I'm able (for the time being) run both my Node app and my website on one droplet with one easy deploy command from my Terminal.

So as you poke around my website, there are obviously a few things I still need to work on and optimize within Gatsby, but by and large working with Gatsby has been a great happy medium between handwritten static HTML, CSS, and Javascript, and a full fledged Create-React-App. I definitely encourage you to explore their docs and even give it a try for your next project!

Want to know more or just have some questions? Drop me a line either via [LinkedIn](https://www.linkedin.com/in/grantstoltz/) or of course through the contact form you see below!