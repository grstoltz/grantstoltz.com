require('dotenv').config()
var express = require('express')
var app = express()
var bodyParser = require('body-parser')
var cors = require('cors')

var PORT = 9001

app.use(cors())

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))

app.set('view engine', 'ejs')

// app.post('/submit', function (req, res) {
//     console.log("Route hit")
//     let mailOpts, smtpTrans;
//     smtpTrans = nodemailer.createTransport({
//       host: 'smtp.gmail.com',
//       port: 465,
//       secure: true,
//       auth: {
//         type: "OAuth2",
//         user: process.env.GMAIL_USER,
//         clientId: process.env.CLIENT_ID,
//         clientSecret: process.env.CLIENT_SECRET,
//         refreshToken: process.env.REFRESH_TOKEN
//       }
//     });
//     mailOpts = {
//       from: req.body.name + ' &lt;' + req.body.email + '&gt;',
//       to: process.env.GMAIL_USER,
//       subject: 'New message from contact form at grantstoltz.com',
//       text: `${req.body.name} (${req.body.email}) says: ${req.body.message}`
//     };
//     smtpTrans.sendMail(mailOpts, function (error, response) {
//       if (error) {
//         //res.render('contact-failure');
//         console.log(error)
//       }
//       else {
//         res.render('contact-success');
//       }
//     });
//   });

app.get('/submit', function(req, res) {
  res.json('route hit')
})

app.post('/submit', function(req, res) {
  console.log(req.body)
  if (
    req.body.message.indexOf('http://') === -1 &&
    req.body.message.indexOf('https://') === -1
  ) {
    var send = require('gmail-send')({
      //var send = require('../index.js')({
      user: process.env.GMAIL_USER,
      // user: credentials.user,                  // Your GMail account used to send emails
      pass: process.env.APP_PASSWORD,
      // pass: credentials.pass,                  // Application-specific password
      to: process.env.GMAIL_USER,
      // to:   credentials.user,                  // Send to yourself
      // you also may set array of recipients:
      // [ 'user1@gmail.com', 'user2@gmail.com' ]
      from: req.body.email, // from: by default equals to user
      replyTo: req.body.email, // replyTo: by default undefined
      // bcc: 'some-user@mail.com',            // almost any option of `nodemailer` will be passed to it
      subject: 'Someone filled out your form!',
      text: `From: ${req.body.name}
      Message: ${req.body.message}`, // Plain text
      //html:    '<b>html text</b>'            // HTML
    })

    var error
    send({}, function(err, res) {
      if (err) {
        console.log(err)
        error = err
      }
    })
    res.redirect('/')
  } else {
    res.redirect('/')
  }
})

app.listen(PORT, function() {
  console.log('Listening on port ' + PORT)
})
