import React from 'react'
import Helmet from 'react-helmet'
import GenericBanner from '../components/GenericBanner'
import Layout from '../components/layout'

import AdPreviewer from '../assets/images/Ad-Previewer.jpg'
import UTMGenerator from '../assets/images/UTM-Generator.jpg'

const Generic = props => (
  <Layout>
    <Helmet>
      <title>Grant Stoltz | Code</title>
      <meta
        name="description"
        content="Here are some examples of some web development projects I've worked on"
      />
    </Helmet>

    <GenericBanner
      header="Coding Projects"
      content="Here are some examples of some web development projects I've worked on"
    />
    <div id="main">
      <section id="one">
        <div className="inner">
          <header className="major">
            <h1 style={{}}>Web Development Projects</h1>
          </header>
          <p>
            Below are some projects that I've created or been a part of
            creating. I primary develop within the MERN stack (MySQL, Express,
            React, and Node). Most of the time the tools or applications I've
            built have stemmed from my own experiences working with existing
            digital marketing tools.
          </p>
        </div>
      </section>
      <section id="two" className="tiles">
        <article style={{ backgroundImage: `url(${AdPreviewer})` }}>
          <header className="major">
            <h3>Facebook Ads Previewer</h3>
            <p>
              Code: <a href="https://github.com/grstoltz/fb-parser">GitHub</a>
            </p>
            <p>
              Deployed:{' '}
              <a href="https://fb-ad-previewer.herokuapp.com">Heroku</a>
            </p>
          </header>
          {/* <Link to="/landing" className="link primary" /> */}
        </article>
        <article style={{ backgroundImage: `url(${UTMGenerator})` }}>
          <header className="major">
            <h3>Facebook Ads UTM Generator</h3>
            <p>
              Code:{' '}
              <a href="https://github.com/grstoltz/fb-utm-generator">GitHub</a>
            </p>
            <p>
              Deployed:{' '}
              <a href="https://fb-utm-generator.herokuapp.com">Heroku</a>
            </p>
          </header>
        </article>
      </section>
    </div>
  </Layout>
)

export default Generic
