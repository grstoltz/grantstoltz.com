import React from 'react'
import PropTypes from 'prop-types'
import { Link } from 'gatsby'

const Menu = props => (
  <nav id="menu">
    <div className="inner">
      <ul className="links">
        <li>
          <Link
            style={{ color: 'inherit' }}
            onClick={props.onToggleMenu}
            to="/"
          >
            Home
          </Link>
        </li>
        {/* <li>
          <Link onClick={props.onToggleMenu} to="/speaking">
            Public Speaking
          </Link>
        </li> */}
        <li>
          <Link
            style={{ color: 'inherit' }}
            onClick={props.onToggleMenu}
            to="/code"
          >
            Code
          </Link>
        </li>
        <li>
          <Link
            style={{ color: 'inherit' }}
            onClick={props.onToggleMenu}
            to="/about"
          >
            About
          </Link>
        </li>
        <li>
          <Link
            style={{ color: 'inherit' }}
            onClick={props.onToggleMenu}
            to="/blog"
          >
            Blog
          </Link>
        </li>
      </ul>
    </div>
    <a
      style={{ boxShadow: 'inset 0 0 0 0px' }}
      className="close"
      onClick={props.onToggleMenu}
      // href="javascript:;"
    >
      Close
    </a>
  </nav>
)

Menu.propTypes = {
  onToggleMenu: PropTypes.func,
}

export default Menu
