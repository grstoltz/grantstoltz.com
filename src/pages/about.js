import React from 'react'
import { Link } from 'gatsby'
import Helmet from 'react-helmet'

import Layout from '../components/layout'

const Generic = props => (
  <Layout>
    <Helmet>
      <title>Grant Stoltz | About</title>
      <meta
        name="description"
        content="Find (almost) everything you wanted to know about Grant Stoltz"
      />
    </Helmet>

    <div id="main" className="alt">
      <section id="one">
        <div className="inner">
          <header className="major">
            <h1>About Me</h1>
          </header>
          {/* <span className="image main"><img src={pic11} alt="" /></span> */}
          <p>
            Let's start this off by saying I won't awkwardly refer to myself in
            the 3rd person for this section of the site.
          </p>
          <p>
            So you want to know more about me? Consider me flattered. Short
            story long, I'm a grduate of The University of Arizona where I
            majored in Business Management with a focus in marketing. After
            college I took my talents just up the road to a company called
            Simpleview, where I currently work on the Digital Strategy team
            helping clients achieve their digital marketing goals.
          </p>
          <p>
            Outside of my work in marketing I love web development, which I hope
            you'll check out in the <Link to="/code">Code</Link> section of the
            site. You might notice that my web development projects are a bit
            self serving for digital marketing, but isn't that how the saying
            goes - Necessity is the mother of invention.
          </p>
          <p>
            Anyways, thanks for dropping by, if you want to get in touch, shoot
            me an email below or fill out the contact form. Either way I'll see
            it!
          </p>
        </div>
      </section>
    </div>
  </Layout>
)

export default Generic
