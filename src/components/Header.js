import React from 'react'
import PropTypes from 'prop-types'
import { Link } from 'gatsby'

const Header = props => (
  <header id="header" className="alt">
    <Link to="/" className="logo">
      <strong>Grant Stoltz</strong>
    </Link>
    <nav>
      <a
        className="menu-link"
        onClick={props.onToggleMenu}
        style={{
          boxShadow: 'inset 0 0 0 0px',
          color: 'inherit',
          textDecoration: 'none',
        }}
        href="javascript:;"
      >
        Menu
      </a>
    </nav>
  </header>
)

Header.propTypes = {
  onToggleMenu: PropTypes.func,
}

export default Header
