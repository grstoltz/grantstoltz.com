import React from 'react'
import MetaTags from '../components/MetaTag'
import { get } from 'lodash'
import Layout from '../components/layout'
import { Link, graphql } from 'gatsby'

class BlogPostTemplate extends React.Component {
  componentDidMount() {
    const $style = document.createElement('style')
    document.head.appendChild($style)
    $style.innerHTML = `#img-wrapper {
      width: 100%;
      text-align: center;
    }
    `
  }

  render() {
    console.log(this.props.location)
    const post = this.props.data.markdownRemark
    const url = 'https://www.grantstoltz.com'
    const thumbnail = post.frontmatter.image.childImageSharp.resize.src
    const { description } = post.frontmatter
    const siteTitle = get(this.props, 'data.site.siteMetadata.title')

    return (
      <Layout>
        <MetaTags
          title={`${post.frontmatter.title}`}
          description={description}
          thumbnail={url + thumbnail}
          url={url}
          pathname={this.props.location.pathname}
        />
        <section id="one" className="white">
          <div className="inner">
            <header className="major" style={{ width: '100%' }}>
              <h1 style={{ color: 'black' }}>{post.frontmatter.title}</h1>
              <h5 id="content">Date Posted: {post.frontmatter.date}</h5>
            </header>
            <div id="img-wrapper" style={{ marginBottom: '2em' }}>
              {/* <Img fluid={post.frontmatter.image.childImageSharp.fluid} /> */}
              <img src={post.frontmatter.image.childImageSharp.fluid.src} />
            </div>

            <p
              dangerouslySetInnerHTML={{ __html: post.html }}
              style={{ color: 'black' }}
            />
            <Link to="/blog">
              <span className="icon fa-long-arrow-left" /> Back to the Blog
            </Link>
          </div>
        </section>
        <hr />
      </Layout>
    )
  }
}

export default BlogPostTemplate

export const pageQuery = graphql`
  query BlogPostByPath($path: String!) {
    site {
      siteMetadata {
        title
        author
      }
    }
    markdownRemark(frontmatter: { path: { eq: $path } }) {
      id
      html
      frontmatter {
        title
        date(formatString: "MMMM DD, YYYY")
        description
        image {
          childImageSharp {
            resize(width: 1500, height: 1500) {
              src
            }
            fluid(maxWidth: 786) {
              ...GatsbyImageSharpFluid
            }
          }
        }
      }
    }
  }
`
