import React from 'react'

const Contact = props => (
  <section style={{ backgroundColor: 'grey' }} id="contact">
    <div className="inner">
      <section>
        <p>{props.status}</p>
        <form method="post" action="/submit">
          <div className="field half first">
            <label htmlFor="name">Name</label>
            <input
              onChange={props.handleChange}
              type="text"
              name="name"
              id="name"
              value={props.name}
            />
          </div>
          <div className="field half">
            <label htmlFor="email">Email</label>
            <input
              onChange={props.handleChange}
              type="text"
              name="email"
              id="email"
              value={props.email}
            />
          </div>
          <div className="field">
            <label htmlFor="message">Message</label>
            <textarea
              onChange={props.handleChange}
              name="message"
              id="message"
              rows="6"
              value={props.message}
            />
          </div>
          <ul className="actions">
            <li>
              <input
                type="submit"
                onClick={props.handleSubmit}
                value="Send Message"
                className="special"
              />
            </li>
            <li>
              <input type="reset" onClick={props.clearForm} value="Clear" />
            </li>
          </ul>
        </form>
      </section>
      <section className="split">
        <section>
          <div className="contact-method">
            <span className="icon alt fa-envelope" />
            <h3>Email</h3>
            <a
              style={{ color: 'inherit', textDecoration: 'none' }}
              href="mailto:hello@grantstoltz.com"
            >
              hello@grantstoltz.com
            </a>
          </div>
        </section>
        <section>
          <div className="contact-method">
            <span className="icon alt fa-phone" />
            <h3>Phone</h3>
            <span>(520) 477-6267</span>
          </div>
        </section>
      </section>
    </div>
  </section>
)

export default Contact
